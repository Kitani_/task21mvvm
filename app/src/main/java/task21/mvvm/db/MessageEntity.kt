package task21.mvvm.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MessageEntity(
        @PrimaryKey
        val id: Int,
        val userId: Int,
        val title: String,
        val text: String
)
