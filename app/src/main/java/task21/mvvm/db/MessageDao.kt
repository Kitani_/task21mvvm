package task21.mvvm.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MessageDao {
    //запити до БД
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(listOfMessageEntity: List<MessageEntity?>)

    @Query("SELECT * FROM MessageEntity")
    fun getMessagesFromDB(): LiveData<List<MessageEntity>>

    @Query("SELECT * FROM MessageEntity WHERE userId LIKE :pickedNumber")
    fun findByUserId(pickedNumber: String)
}