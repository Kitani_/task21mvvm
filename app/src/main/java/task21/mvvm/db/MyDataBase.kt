package task21.mvvm.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [MessageEntity::class], version = 1)
abstract class MyDataBase: RoomDatabase() {
    abstract fun messageDao(): MessageDao?
    companion object {
        private var instance: MyDataBase? = null
        @JvmStatic
        @Synchronized
        fun getInstance(context: Context): MyDataBase {
            if (MyDataBase.Companion.instance == null) {
                MyDataBase.Companion.instance = Room.databaseBuilder(context.applicationContext,
                        MyDataBase::class.java, "my_database")
                        .fallbackToDestructiveMigration()
                        .addCallback(MyDataBase.Companion.roomCallback)
                        .build()
            }
            return MyDataBase.Companion.instance!!
        }

        private val roomCallback: Callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }
    }
}

/*package task21.mvvm.db
@Database(entities = [MessageEntity::class], version = 1)
abstract class MyDataBase : RoomDatabase() {
    abstract fun messageDao(): MessageDao?

    companion object {
        private val instance: MyDataBase? = null
        @Synchronized
        fun getInstance(context: Context): MyDataBase {
            if (MyDataBase.Companion.instance == null) {
                MyDataBase.Companion.instance = Room.databaseBuilder(context.applicationContext,
                        MyDataBase::class.java, "my_database")
                        .fallbackToDestructiveMigration()
                        .addCallback(MyDataBase.Companion.roomCallback)
                        .build()
            }
            return MyDataBase.Companion.instance
        }

        private val roomCallback: Callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }
    }
}*/