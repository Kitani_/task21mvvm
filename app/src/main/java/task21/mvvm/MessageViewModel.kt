package task21.mvvm


import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import task21.mvvm.db.MessageEntity

class MessageViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: MessageRepository = MessageRepository(application)
    val allMessages: LiveData<List<MessageEntity>> = repository.allMessages

    fun update() {
        viewModelScope.launch(IO) {
            repository.fetchAndInsertData()
        }
    }
}