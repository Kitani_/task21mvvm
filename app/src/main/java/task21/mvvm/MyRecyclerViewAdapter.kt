package task21.mvvm

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import task21.mvvm.MyRecyclerViewAdapter.MyViewHolder
import task21.mvvm.db.MessageEntity

class MyRecyclerViewAdapter(private val context: Context) : RecyclerView.Adapter<MyViewHolder>() {
    private var messageEntities: List<MessageEntity>? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item, parent, false)
        return MyViewHolder(view)
    }

    //тут заповнюється айтем
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val (id, userId, title, text) = messageEntities!![position]
        holder.autoId.text = "Post ID$id"
        holder.userIdTextView.text = "User ID$userId"
        holder.titleTextView.text = title
        holder.bodyTextView.text = text
    }

    override fun getItemCount(): Int {
        return if (messageEntities == null) {
            0
        } else {
            messageEntities!!.size
        }
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val autoId: TextView = itemView.findViewById(R.id.idTextView)
        val userIdTextView: TextView = itemView.findViewById(R.id.userIDTextView)
        val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)
        val bodyTextView: TextView = itemView.findViewById(R.id.bodyTextView)

    }

    fun setItems(messageEntities: List<MessageEntity>?) {
        this.messageEntities = messageEntities
        notifyDataSetChanged()
    }
}