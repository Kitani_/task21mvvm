package task21.mvvm

import android.app.Application
import androidx.lifecycle.LiveData
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import task21.mvvm.api.JsonPlaceHolderApi
import task21.mvvm.api.MessageApiModel
import task21.mvvm.db.MessageDao
import task21.mvvm.db.MessageEntity
import task21.mvvm.db.MyDataBase.Companion.getInstance
import java.util.*

class MessageRepository(application: Application?) {
    private var messageDao: MessageDao

    private lateinit var jsonPlaceHolderApi: JsonPlaceHolderApi

    val allMessages: LiveData<List<MessageEntity>>

    init {
        val myDataBase = getInstance(application!!)
        messageDao = myDataBase.messageDao()!!
        allMessages = messageDao.getMessagesFromDB()
    }

    suspend fun fetchAndInsertData() {
        val fetchedApiModels = fetchApiModels()
        val convertedList = convertApiToEntity(fetchedApiModels)
        messageDao.insert(convertedList)
    }

    private fun convertApiToEntity(messages: List<MessageApiModel>): List<MessageEntity> {
        val convertedList: MutableList<MessageEntity> = ArrayList()
        for (message in messages) {
            val messageEntity = MessageEntity(
                id = message.id,
                userId = message.userId,
                title = message.title,
                text = message.text
            )

            convertedList.add(messageEntity)
        }
        return convertedList
    }

    private suspend fun fetchApiModels(): List<MessageApiModel> {

        val retrofit = Retrofit.Builder()
            .baseUrl("http://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi::class.java)

        val call = jsonPlaceHolderApi.messages.awaitResponse()
        val messageApiModels = call.body()!!
        return messageApiModels
    }
}