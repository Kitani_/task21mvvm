package task21.mvvm

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var messageViewModel: MessageViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val updateTheListButton = findViewById<Button>(R.id.updateButton)
        updateTheListButton.setOnClickListener(this)

        val recyclerView = findViewById<RecyclerView>(R.id.myRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        val adapter = MyRecyclerViewAdapter(this)
        recyclerView.adapter = adapter

        messageViewModel = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(this.application)).get(MessageViewModel::class.java)
        messageViewModel.allMessages.observe(this, { messageEntities ->
            Toast.makeText(this@MainActivity, "Updated", Toast.LENGTH_SHORT).show()
           adapter.setItems(messageEntities)
        })
    }

    override fun onClick(v: View) {
        messageViewModel.update()
    }
}