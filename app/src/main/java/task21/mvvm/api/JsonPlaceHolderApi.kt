package task21.mvvm.api

import retrofit2.Call
import retrofit2.http.GET

interface JsonPlaceHolderApi {
    //запит на сервер
    @get:GET("posts")
    val messages: Call<List<MessageApiModel>>
}
