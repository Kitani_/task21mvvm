package task21.mvvm.api

import com.google.gson.annotations.SerializedName

data class MessageApiModel (

        val id: Int,
        val userId: Int,
        val title: String,
        @SerializedName("body") val text: String
)
